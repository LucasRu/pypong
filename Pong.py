import turtle

# Init

wn = turtle.Screen()
wn.title("Pong by @LucasRu")
wn.bgcolor("black")
wn.setup(width=800, height=600)
wn.tracer(0)


# Text

class Text:

    def __init__(self, content, x, y):
        self.text = turtle.Turtle(visible=False)
        self.x = x
        self.y = y
        self.text.setposition(x, y)
        self.text.color("white")
        self.text.write(content, align='center', font='Arial')

    def update(self, content):
        self.text.undo()
        self.text.setposition(self.x, self.y)
        self.text.color("white")
        self.text.write(content, align="center", font="Arial")


title = Text("PyPong", 0, 250)


# Paddle

class Paddle:

    def __init__(self, x, c):
        self.paddle = turtle.Turtle()
        self.paddle.speed(0)
        self.paddle.shape("square")
        self.paddle.shapesize(stretch_wid=5, stretch_len=1)
        self.paddle.penup()
        self.paddle.color(c)
        self.paddle.goto(x, 0)

    def move_up(self):
        self.paddle.sety(self.paddle.ycor() + 20)

    def move_down(self):
        self.paddle.sety(self.paddle.ycor() - 20)

    def move_right(self):
        self.paddle.setx(self.paddle.xcor() + 20)

    def move_left(self):
        self.paddle.setx(self.paddle.xcor() - 20)

    def gety(self):
        return self.paddle.ycor()

    def getx(self):
        x = self.paddle.xcor()
        return x


paddle_a = Paddle(-350, "cyan")
paddle_b = Paddle(350, "yellow")


# Ball

class Ball:

    def __init__(self):
        self.ball = turtle.Turtle()
        self.ball.speed("slow")
        self.ball.shape("circle")
        self.ball.penup()
        self.ball.color("Pink")
        self.ball.goto(0, 0)
        self.dx = 0.5
        self.dy = 0.5

    def get_ycor(self):
        return self.ball.ycor()

    def get_xcor(self):
        return self.ball.xcor()

    def go_to(self, x, y):
        self.ball.goto(x, y)

    def bounce(self, axis):
        if axis == "x":
            self.dx *= -1
        if axis == "y":
            self.dy *= -1

    def move(self):
        dx = self.ball.xcor() + self.dx
        dy = self.ball.ycor() + self.dy
        self.ball.goto(dx, dy)


ball = Ball()


# Game Logic

class Game:

    def __init__(self):
        self.a_score = 0
        self.b_score = 0
        self.title = Text("PyPong", 0, 250)
        self.score_a = Text(self.a_score, -350, 250)
        self.score_b = Text(self.b_score, 350, 250)

    def score(self, player):
        if player == 0:
            self.a_score += 1
            self.score_a.update(self.a_score)
        if player == 1:
            self.b_score += 1
            self.score_b.update(self.b_score)

    def collision(self):
        # paddle wall collision

        if ball.get_ycor() > 290:
            ball.go_to(ball.get_xcor(), 290)
            ball.bounce("y")

        if ball.get_ycor() < -290:
            ball.go_to(ball.get_xcor(), -290)
            ball.bounce("y")

        if ball.get_xcor() > 390:
            ball.go_to(0, 0)
            ball.bounce("x")

        if ball.get_xcor() < -390:
            ball.go_to(0, 0)
            ball.bounce("x")

        # paddle ball collision

        ax = paddle_a.getx() + 20
        bx = paddle_b.getx() - 20
        ay = paddle_a.gety()
        by = paddle_b.gety()

        if ay + 80 > ball.get_ycor() > ay - 80 and ball.get_xcor() == ax or by + 80 > ball.get_ycor() > by - 80 and ball.get_xcor() == bx:
            ball.bounce("x")

        # paddle goal collision

        if ball.get_xcor() == 390:
            ball.go_to(0, 0)
            ball.bounce("x")
            self.score(0)

        if ball.get_xcor() == -390:
            ball.go_to(0, 0)
            ball.bounce("x")
            self.score(1)


game = Game()

# Keyboard binding

wn.listen()
wn.onkeypress(paddle_a.move_up, "w")
wn.onkeypress(paddle_a.move_down, "s")
wn.onkeypress(paddle_b.move_up, "Up")
wn.onkeypress(paddle_b.move_down, "Down")

wn.onkeypress(paddle_a.move_right, "d")
wn.onkeypress(paddle_a.move_left, "a")
wn.onkeypress(paddle_b.move_right, "Right")
wn.onkeypress(paddle_b.move_left, "Left")

# Main Game Loop

while True:
    wn.update()

    # Ball Movement
    ball.move()
    game.collision()
